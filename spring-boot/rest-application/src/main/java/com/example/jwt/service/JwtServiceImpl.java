package com.example.jwt.service;

import com.example.common.util.TimeUtils;
import com.example.jwt.api.JwtService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.jackson.io.JacksonDeserializer;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class JwtServiceImpl implements JwtService {

    private static final String PAYLOAD_FIELD = "payload";

    @Value("${security.jwt.token.secret-key}")
    private String secretKey;

    @Override
    public <T> T parseToken(String token, Class<T> clazz) {
        Jws<Claims> jws = Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor(secretKey.getBytes()))
                .deserializeJsonWith(new JacksonDeserializer<>(Map.of(PAYLOAD_FIELD, clazz)))
                .build()
                .parseClaimsJws(token);
        Claims body = jws.getBody();
        return body.get(PAYLOAD_FIELD, clazz);
    }

    @Override
    public <T> String createToken(T payload, LocalDateTime expiration) {
        Date date = Date.from(TimeUtils.timeToInstant(expiration));
        Claims claims = Jwts.claims();
        claims.put(PAYLOAD_FIELD, payload);
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date())
                .setExpiration(date)
                .signWith(Keys.hmacShaKeyFor(secretKey.getBytes()), SignatureAlgorithm.HS256)
                .compact();
    }
}
