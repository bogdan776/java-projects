package com.example.jwt.api;

import java.time.LocalDateTime;

public interface JwtService {

    <T> T parseToken(String token, Class<T> clazz);

    <T> String createToken(T payload, LocalDateTime expiration);
}
