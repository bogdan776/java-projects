package com.example.rest.auth.controller.action;

import com.example.rest.auth.api.usecase.action.SignUpUseCase;
import com.example.rest.auth.dto.request.SignUpRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@RequiredArgsConstructor
public class SignUpController {

    private final SignUpUseCase signUp;

    @PostMapping(value = "/v1/signup", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> signUp(
            @Validated @RequestBody SignUpRequest request
    ) {
        signUp.execute(request.getEmail(), request.getPassword());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
