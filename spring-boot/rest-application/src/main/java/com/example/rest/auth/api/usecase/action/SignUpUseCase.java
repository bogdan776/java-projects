package com.example.rest.auth.api.usecase.action;

public interface SignUpUseCase {

    void execute(String email, String password);
}
