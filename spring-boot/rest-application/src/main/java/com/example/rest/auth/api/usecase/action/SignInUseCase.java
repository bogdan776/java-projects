package com.example.rest.auth.api.usecase.action;

public interface SignInUseCase {

    void execute(String email, String password);
}
