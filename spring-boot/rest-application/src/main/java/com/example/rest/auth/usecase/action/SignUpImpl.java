package com.example.rest.auth.usecase.action;

import com.example.rest.auth.api.usecase.action.SignUpUseCase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SignUpImpl implements SignUpUseCase {

    @Override
    public void execute(String email, String password) {
        log.info("email: " + email);
        log.info("password: " + password);
    }
}
