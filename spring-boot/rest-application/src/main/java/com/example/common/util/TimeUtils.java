package com.example.common.util;

import org.springframework.lang.Nullable;

import java.time.*;
import java.time.temporal.ChronoUnit;

public final class TimeUtils {

    public static final ZoneId ZONE_ID = ZoneId.of("UTC");
    public static final ZoneOffset ZONE_OFFSET = ZoneOffset.UTC;

    private TimeUtils() {
    }

    public static long timeToMilli(LocalDateTime time) {
        return time.toInstant(ZONE_OFFSET).toEpochMilli();
    }

    public static Instant timeToInstant(LocalDateTime time) {
        return time.toInstant(ZONE_OFFSET);
    }

    public static LocalDateTime timeFromMilli(Long time) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZONE_ID);
    }

    @Nullable
    public static LocalDateTime timeFromMilliNullable(@Nullable Long time) {
        return time == null ? null : LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZONE_ID);
    }

    public static LocalDateTime timeNow() {
        return LocalDateTime.now(ZONE_ID);
    }

    public static LocalDateTime datePlusTimeNow(LocalDate date) {
        return LocalDateTime.of(date, LocalTime.now(ZONE_ID));
    }

    public static LocalDate currentDate() {
        return LocalDate.now(ZONE_ID);
    }

    public static LocalDateTime todayEnd() {
        return LocalDateTime.of(currentDate(), LocalTime.MAX).withNano(0);
    }

    public static LocalDateTime todayStart() {
        return currentDate().atStartOfDay();
    }

    public static LocalDateTime dateEnd(LocalDate date) {
        return LocalDateTime.of(date, LocalTime.MAX).withNano(0);
    }

    public static LocalDateTime dateStart(LocalDate date) {
        return date.atStartOfDay();
    }

    public static LocalDateTime monthEnd(LocalDate date) {
        return LocalDateTime.of(date, LocalTime.MAX).withDayOfMonth(date.lengthOfMonth());
    }

    public static LocalDateTime monthStart(LocalDate date) {
        return LocalDateTime.of(date, LocalTime.MIN).withDayOfMonth(1);
    }

    public static LocalDateTime timeNowPlusMilli(Long milliseconds) {
        return LocalDateTime.now(ZONE_ID).plus(milliseconds, ChronoUnit.MILLIS);
    }

    public static LocalDateTime timeNowMinusMilli(Long milliseconds) {
        return LocalDateTime.now(ZONE_ID).minus(milliseconds, ChronoUnit.MILLIS);
    }

    public static long timeNowInMilli() {
        return Instant.now().toEpochMilli();
    }

    public static Integer getYears(LocalDate date) {
        return Period.between(date, LocalDate.now()).getYears();
    }
}
