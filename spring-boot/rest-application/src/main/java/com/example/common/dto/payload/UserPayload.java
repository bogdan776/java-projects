package com.example.common.dto.payload;

import com.example.common.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPayload {

    private UUID uuid;

    private UserRole userRole;
}
