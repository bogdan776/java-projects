package com.example.security;

import com.example.common.dto.payload.UserPayload;
import com.example.jwt.api.JwtService;
import com.example.security.exception.TokenValidationException;
import com.example.security.token.JwtAuthToken;
import com.example.security.token.UserContext;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class JwtAuthProvider implements AuthenticationProvider {

    private final JwtService jwtService;

    public JwtAuthToken validateToken(String token) {
        try {
            UserContext userContext = UserContext.of(jwtService.parseToken(token, UserPayload.class));
            return new JwtAuthToken(userContext, userContext.getRole());
        } catch (ExpiredJwtException ex) {
            throw new TokenValidationException("Authorization token expired");
        } catch (Exception ex) {
            throw new TokenValidationException("Invalid authorization token");
        }
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return JwtAuthToken.class.isAssignableFrom(aClass);
    }
}
