package com.example.security.token;

import com.example.common.dto.payload.UserPayload;
import com.example.common.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserContext {

    private UUID uuid;

    private UserRole role;

    public static UserContext of(UserPayload userPayload) {
        return new UserContext(userPayload.getUuid(), userPayload.getUserRole());
    }
}
