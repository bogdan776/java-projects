package com.example.security.filter;

import com.example.security.token.JwtAuthToken;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.function.Function;

@AllArgsConstructor
public class JwtTokenFilter extends OncePerRequestFilter {

    private final Function<String, JwtAuthToken> tokenFunction;


    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    FilterChain filterChain
    ) throws ServletException, IOException {
        String token = httpServletRequest.getHeader("Authorization");

        if (token != null) {
            if (!token.startsWith("Bearer ")) {
                throw new IllegalStateException("Missing Authentication Token");
            }

            JwtAuthToken jwtAuthenticationToken = tokenFunction.apply(token.replace("Bearer ", ""));

            SecurityContextHolder.getContext().setAuthentication(jwtAuthenticationToken);
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
