package com.example.security.token;

import lombok.EqualsAndHashCode;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;

import java.util.Collection;
import java.util.Collections;

@EqualsAndHashCode(callSuper = true)
public class JwtAuthToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

    private transient UserCredentials userCredentials;
    private transient UserContext userContext;

    public JwtAuthToken(UserCredentials credentials) {
        super(null);
        this.userCredentials = credentials;
        this.setAuthenticated(false);
    }

    public JwtAuthToken(UserContext context, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.eraseCredentials();
        this.userContext = context;
        super.setAuthenticated(true);
    }

    public JwtAuthToken(UserContext context, GrantedAuthority authority) {
        super(Collections.singleton(authority));
        this.eraseCredentials();
        this.userContext = context;
        super.setAuthenticated(true);
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        if (authenticated) {
            throw new IllegalArgumentException("Cannot set this token");
        }
        super.setAuthenticated(false);
    }

    @Override
    public String getName() {
        return userContext.getUuid().toString();
    }

    @Override
    public Object getCredentials() {
        return userCredentials;
    }

    @Override
    public Object getPrincipal() {
        return this.userContext;
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
        this.userCredentials = null;
    }
}
