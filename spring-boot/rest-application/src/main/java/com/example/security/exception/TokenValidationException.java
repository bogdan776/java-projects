package com.example.security.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class TokenValidationException extends SecurityException{

    public TokenValidationException() {
        super();
    }

    public TokenValidationException(String message) {
        super(message);
    }
}
