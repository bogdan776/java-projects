package com.example.security.exception;

import com.example.exception.ApplicationException;

public abstract class SecurityException extends ApplicationException {

    public SecurityException() {
        super();
    }

    public SecurityException(String message) {
        super(message);
    }

    public SecurityException(Throwable cause) {
        super(cause);
    }
}
