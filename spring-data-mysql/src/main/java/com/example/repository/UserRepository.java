package com.example.repository;

import com.example.entity.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByLogin(String login);

    @Modifying
    @Query(value = "insert into user_has_role(user_id, role_id) VALUES (:user_id, :role_id)", nativeQuery = true)
    @Transactional
    void setRoles(@Param("user_id") Long userId, @Param("role_id") Long role_Id);
}
