package com.example;

import com.example.entity.User;
import com.example.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Slf4j
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(UserRepository repository) {
        return (args -> {
            log.info("create users");
            User u1 = new User("U3", "P3", "L3");
            User u2 = new User("U4", "P4", "L4");
            repository.save(u1);
            repository.save(u2);

            log.info("add roles");
            repository.setRoles(u1.getId(), 1L);
            repository.setRoles(u2.getId(), 2L);

            log.info("find user by login");
            log.info(repository.findByLogin("L3").get().toString());

            log.info("find all");
            repository.findAll().forEach(u -> log.info(u.toString()));
        });
    }
}
