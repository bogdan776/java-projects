create table users
(
    id       bigint auto_increment primary key,
    name     varchar(255) not null,
    login    varchar(255) not null,
    password varchar(255) not null
);

create table roles
(
    id   bigint primary key not null,
    role varchar(45)        not null
);

create table user_has_role
(
    user_id bigint not null,
    role_id bigint not null,
    primary key (user_id, role_id)
);
