package org.collection.list;

import org.collection.MyCollection;
import org.collection.MyIterator;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

public interface MyList<E> extends MyCollection<E> {

    int size();

    boolean isEmpty();

    boolean contains(Object o);

    Object[] toArray();

    <T> T[] toArray(T[] a);

    boolean add(E e);

    boolean remove(Object o);

    boolean containsAll(MyCollection<?> c);

    boolean addAll(MyCollection<? extends E> c);

    boolean removeAll(MyCollection<?> c);

    void clear();

    MyIterator<E> iterator();

    boolean equals(Object o);

    int hashCode();

    boolean containsAll(Collection<?> c);

    boolean addAll(int index, MyCollection<? extends E> c);

    E get(int index);

    E set(int index, E element);

    void add(int index, E element);

    E remove(int index);

    int indexOf(Object o);

    int lastIndexOf(Object o);

    MyListIterator<E> listIterator();

    @SuppressWarnings({"unchecked", "rawtypes"})
    default void sort(Comparator<? super E> c) {
        Object[] a = this.toArray();
        Arrays.sort(a, (Comparator) c);
        MyListIterator<E> i = this.listIterator();
        for (Object e : a) {
            i.next();
            i.set((E) e);
        }
    }
}
