package org.collection.list;

import org.collection.MyIterator;

public interface MyListIterator<E> extends MyIterator<E> {

    @Override
    boolean hasNext();

    @Override
    E next();

    @Override
    void remove();

    boolean hasPrevious();

    E previous();

    int nextIndex();

    int previousIndex();

    void set(E e);

    void add(E e);
}
