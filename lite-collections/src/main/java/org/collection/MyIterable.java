package org.collection;

public interface MyIterable<E> {

    MyIterator<E> iterator();
}
