package org.collection.queue;

import org.collection.MyCollection;

public interface MyQueue<E> extends MyCollection<E> {

    boolean add(E e);

    boolean offer(E e);

    E remove();

    E poll();

    E element();

    E peek();
}
