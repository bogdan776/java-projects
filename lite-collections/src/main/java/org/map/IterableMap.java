package org.map;

import java.util.Iterator;
import java.util.Map;

/**
 * Map with iteration function.
 * <p>
 * You can see what kind of life would be, if {@link Map} implemented iterator...
 *
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 * @author Bohdan Pereverziev
 */
public interface IterableMap<K, V> extends LiteMap<K, V>, Iterable<LiteMap.Entry<K, V>> {

    @Override
    Iterator<Entry<K, V>> iterator();
}
