package org.map;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Deprecated
public class LiteHashMap<K, V> implements LiteMap<K, V> {

    private Node<K, V>[] table;

    private int size;

    int threshold;

    private final float loadFactor;

    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    private static final int MAXIMUM_CAPACITY = 1 << 30;

    private static final int DEFAULT_INITIAL_CAPACITY = 1 << 4;

    public LiteHashMap() {
        this.loadFactor = DEFAULT_LOAD_FACTOR;
        this.table = defaultTable();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return getNode(hash(key), key) != null;
    }

    @Override
    public boolean containsValue(Object value) {
        Node<K, V>[] tab = table;
        V v;
        if (tab != null && size > 0) {
            for (Node<K, V> kvNode : tab) {
                if (kvNode != null)
                    if ((v = kvNode.value) == value || (value != null && value.equals(v)))
                        return true;
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        Node<K, V> e;
        return (e = getNode(hash(key), key)) == null ? null : e.value;
    }

    @Override
    public V put(K key, V value) {
        return putVal(hash(key), key, value);
    }

    @Override
    public V remove(Object key) {
        Node<K, V> e;
        return (e = removeNode(hash(key), key)) == null ? null : e.value;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        putMapEntries(m);
    }

    @Override
    public void clear() {
        Node<K, V>[] tab = table;
        if (tab != null && size > 0) {
            size = 0;
            Arrays.fill(tab, null);
        }
    }

    @Override
    public List<? extends Entry<K, V>> entryList() {
        List<Node<K, V>> nodes = new ArrayList<>();
        for (Node<K, V> entry : table) {
            if (entry != null) {
                if (!nodes.contains(entry)) {
                    nodes.add(entry);
                }
            }
        }
        return nodes;
    }

    @Override
    public String toString() {
        if (size != 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            Object[] tab = entryList().toArray();
            for (int i = 0; ; i++) {
                sb.append(tab[i]);
                if (i == tab.length - 1)
                    return sb.append("}").toString();
                sb.append(", ");
            }
        }
        return "{}";
    }

    public static int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    private Node<K, V> newNode(int hash, K key, V value) {
        return new Node<>(hash, key, value);
    }

    private V putVal(int hash, K key, V value) {
        Node<K, V>[] tab;
        Node<K, V> p;
        int n, i;
        if ((tab = table) == null || (n = tab.length) == 0)
            n = (tab = resize()).length;
        if ((p = tab[i = (n - 1) & hash]) == null)
            tab[i] = newNode(hash, key, value);
        else {
            Node<K, V> e = null;
            K k;
            if (p.hash == hash && ((k = p.key) == key || (key != null && key.equals(k))))
                e = p;
            else {
                p = newNode(hash, key, value);
            }
            if (e != null) {
                V oldValue = e.value;
                if (oldValue == null)
                    e.value = value;
                return oldValue;
            }
        }
        if (++size > threshold)
            resize();
        return null;
    }

    private Node<K, V>[] resize() {
        Node<K, V>[] oldTab = table;
        int oldCap = (oldTab == null) ? 0 : oldTab.length;
        int oldThr = threshold;
        int newCap, newThr = 0;
        if (oldCap > 0) {
            if (oldCap >= MAXIMUM_CAPACITY) {
                threshold = Integer.MAX_VALUE;
                return oldTab;
            } else if (((newCap = oldCap << 1) < MAXIMUM_CAPACITY) && (oldCap >= DEFAULT_INITIAL_CAPACITY))
                newThr = oldThr << 1;
        } else if (oldThr > 0)
            newCap = oldThr;
        else {
            newCap = DEFAULT_INITIAL_CAPACITY;
            newThr = (int) (DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
        }
        if (newThr == 0) {
            float ft = (float) newCap * loadFactor;
            newThr = (newCap < MAXIMUM_CAPACITY && ft < (float) MAXIMUM_CAPACITY ? (int) ft : Integer.MAX_VALUE);
        }
        threshold = newThr;
        @SuppressWarnings("unchecked")
        Node<K, V>[] newTab = (Node<K, V>[]) new Node[newCap];
        if (oldTab != null) {
            for (int j = 0; j < oldCap; j++) {
                Node<K, V> e;
                if ((e = oldTab[j]) != null) {
                    oldTab[j] = null;
                    newTab[e.hash & (newCap - 1)] = e;
                }
            }
        }
        table = newTab;
        return newTab;
    }

    private Node<K, V> getNode(int hash, Object key) {
        Node<K, V>[] tab;
        Node<K, V> first;
        int n;
        K k;
        if ((tab = table) != null && (n = tab.length) > 0 && (first = tab[(n - 1) & hash]) != null) {
            if (first.hash == hash && ((k = first.key) == key || (key != null && key.equals(k))))
                return first;
        }
        return null;
    }

    private Node<K, V> removeNode(int hash, Object key) {
        Node<K, V>[] tab;
        Node<K, V> p;
        int n;
        if ((tab = table) != null && (n = tab.length) > 0 && (p = tab[(n - 1) & hash]) != null) {
            Node<K, V> node = null;
            K k;
            if (p.hash == hash && ((k = p.key) == key || (key != null && key.equals(k))))
                node = p;
            if (node != null) {
                --size;
                return node;
            }
        }
        return null;
    }

    private void putMapEntries(Map<? extends K, ? extends V> m) {
        int s = m.size();
        if (s > 0) {
            if (table == null) {
                float ft = ((float) s / loadFactor) + 1.0F;
                int t = ((ft < (float) MAXIMUM_CAPACITY) ? (int) ft : MAXIMUM_CAPACITY);
                if (t > threshold) threshold = tableSizeFor(t);
            } else if (s > threshold)
                resize();
            for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
                K key = e.getKey();
                V value = e.getValue();
                putVal(hash(key), key, value);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private Node<K, V>[] defaultTable() {
        return new Node[0];
    }

    private static int tableSizeFor(int cap) {
        int n = cap - 1;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
    }

    @Data
    @AllArgsConstructor
    static class Node<K, V> implements Entry<K, V> {

        private final int hash;
        private final K key;
        private V value;

        @Override
        public final int hashCode() {
            return Objects.hashCode(key) ^ Objects.hashCode(value);
        }

        @Override
        public final boolean equals(Object o) {
            if (o == this)
                return true;
            if (o instanceof Entry) {
                Entry<?, ?> e = (Entry<?, ?>) o;
                return Objects.equals(key, e.getKey()) && Objects.equals(value, e.getValue());
            }
            return false;
        }

        @Override
        public String toString() {
            return key + "=" + value;
        }
    }
}
