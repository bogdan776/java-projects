package org.map;

import java.util.List;
import java.util.Map;

/**
 * Simplified version of {@link Map}
 *
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 * @author Bohdan Pereverziev
 */
public interface LiteMap<K, V> {

    int size();

    boolean isEmpty();

    boolean containsKey(Object key);

    boolean containsValue(Object value);

    V get(Object key);

    V put(K key, V value);

    V remove(Object key);

    void putAll(Map<? extends K, ? extends V> m);

    void clear();

    List<? extends LiteMap.Entry<K, V>> entryList();

    interface Entry<K, V> {

        K getKey();

        V getValue();

        boolean equals(Object o);

        int hashCode();
    }
}
