package org.map;

import java.util.HashMap;
import java.util.Map;

public class MapRunner {

    public static void main(String[] args) {
        LiteMap<String, Integer> map = new LiteHashMap<>();
        map.put("Zero", 0);
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);

        Map<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Three", 3);
        hashMap.put("Four", 4);
        hashMap.put("Five", 5);

        map.putAll(hashMap);

        for (LiteMap.Entry<String, Integer> entry : map.entryList()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}
