package org.map;

import java.util.HashMap;
import java.util.Map;

public class IterableMapRunner {

    public static void main(String[] args) {
        IterableMap<String, Integer> map = new IterableLiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);

        Map<String, Integer> map1 = new HashMap<>();
        map1.put("Two", 2);
        map1.put("Three", 3);
        map1.put("Four", 4);
        map1.put("Five", 5);

        map.putAll(map1);

        for (LiteMap.Entry<String, Integer> entry : map) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}
