package org.map;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

public final class IterableLiteHashMap<K, V> implements IterableMap<K, V> {

    private Node<K, V>[] table;

    private int size;

    private static final int MAXIMUM_CAPACITY = 1 << 30;

    private static final int DEFAULT_CAPACITY = 1 << 4;

    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    private int threshold;

    public IterableLiteHashMap() {
        this.table = createArray(0);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return getNode(hash(key), key) != null;
    }

    @Override
    public boolean containsValue(Object value) {
        Node<K, V>[] tab;
        V v;
        if ((tab = table) != null && size > 0) {
            for (Node<K, V> kvNode : tab) {
                for (Node<K, V> e = kvNode; e != null; e = e.next) {
                    if ((v = e.value) == value || (value != null && value.equals(v))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        Node<K, V> e;
        return (e = getNode(hash(key), key)) == null ? null : e.value;
    }

    @Override
    public V put(K key, V value) {
        return putVal(hash(key), key, value);
    }

    @Override
    public V remove(Object key) {
        Node<K, V> e;
        return (e = removeNode(hash(key), key)) == null ? null : e.value;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        if (m.size() != 0) {
            for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
                put(entry.getKey(), entry.getValue());
            }
        }
    }

    @Override
    public void clear() {
        if (size != 0) {
            Arrays.fill(table, null);
            size = 0;
        }
    }

    @Override
    public List<? extends Entry<K, V>> entryList() {
        if (size != 0) {
            List<Node<K, V>> entryList = new ArrayList<>();
            for (Node<K, V> n : table) {
                if (n != null) {
                    entryList.add(n);
                    if (n.next != null) {
                        do {
                            n = n.next;
                            entryList.add(n);
                        } while (n.next != null);
                    }
                }
            }
            return entryList;
        }
        return null;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return new Iterator<Entry<K, V>>() {

            private int cursor = 0;

            private int index = 0;

            private Node<K, V> e = null;

            @Override
            public boolean hasNext() {
                return cursor < size;
            }

            @Override
            public Node<K, V> next() {
                if (!hasNext())
                    throw new NoSuchElementException();
                if (e != null && (e = e.next) != null) {
                    cursor++;
                    return e;
                }
                do {
                    e = table[index++];
                } while (e == null && index != table.length - 1);
                cursor++;
                return e;
            }
        };
    }

    @Override
    public String toString() {
        if (size == 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Node<K, V> n : table) {
            if (n != null) {
                sb.append(n).append(", ");
                if (n.next != null) {
                    do {
                        n = n.next;
                        sb.append(n).append(", ");
                    } while (n.next != null);
                }
            }
        }
        sb.delete(sb.length() - 2, sb.length()).append("}");
        return sb.toString();
    }

    private static int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    private Node<K, V> removeNode(int hash, Object key) {
        Node<K, V>[] tab;
        Node<K, V> p;
        int n, index;
        if ((tab = table) != null && (n = tab.length) > 0 && (p = tab[index = (n - 1) & hash]) != null) {
            Node<K, V> node = null, e;
            K k;
            if (p.hash == hash && ((k = p.key) == key || (key != null && key.equals(k))))
                node = p;
            else if ((e = p.next) != null) {
                do {
                    if (e.hash == hash &&
                            ((k = e.key) == key ||
                                    (key != null && key.equals(k)))) {
                        node = e;
                        break;
                    }
                    p = e;
                } while ((e = e.next) != null);
            }
            if (node != null) {
                if (node == p)
                    tab[index] = node.next;
                else
                    p.next = node.next;
                --size;
                return node;
            }
        }
        return null;
    }

    private V putVal(int hash, K key, V value) {
        Node<K, V>[] tab;
        Node<K, V> node;
        int n, i;
        if ((tab = table) == null || (n = tab.length) == 0) {
            n = resize().length;
        }
        if (n != 0) {
            if ((node = table[i = (n - 1) & hash]) == null) {
                table[i] = newNode(hash, key, value);
            } else {
                if (!node.key.equals(key)) {
                    if (node.next != null) {
                        do {
                            node = node.next;
                        } while (node.next != null);
                    }
                    node.next = newNode(hash, key, value);
                } else {
                    Node<K, V> e = null;
                    K k;
                    if (node.hash == hash && ((k = node.key) == key || key.equals(k)))
                        e = node;
                    if (e != null) {
                        V oldValue = e.value;
                        if (oldValue != null) {
                            e.value = value;
                            return oldValue;
                        }
                    }
                }
            }
        }
        size++;
        return null;
    }

    private Node<K, V> getNode(int hash, Object key) {
        Node<K, V>[] tab;
        Node<K, V> first, e;
        int n;
        K k;
        if ((tab = table) != null && (n = tab.length) > 0 && (first = tab[(n - 1) & hash]) != null) {
            if (first.hash == hash && ((k = first.key) == key || (key != null && key.equals(k)))) {
                return first;
            }
            if ((e = first.next) != null) {
                do {
                    if (e.hash == hash && ((k = e.key) == key || (key != null && key.equals(k))))
                        return e;
                } while ((e = e.next) != null);
            }
        }
        return null;
    }

    private Node<K, V> newNode(int hash, K key, V value) {
        return new Node<>(hash, key, value, null);
    }

    private Node<K, V>[] resize() {
        Node<K, V>[] oldTab = table;
        int oldCap = (oldTab == null) ? 0 : oldTab.length;
        int oldThr = threshold;
        int newCap, newThr = 0;
        if (oldCap > 0) {
            if (oldCap >= MAXIMUM_CAPACITY) {
                threshold = Integer.MAX_VALUE;
                return oldTab;
            } else if ((newCap = oldCap << 1) < MAXIMUM_CAPACITY && oldCap >= DEFAULT_CAPACITY)
                newThr = oldThr << 1;
        } else if (oldThr > 0)
            newCap = oldThr;
        else {
            newCap = DEFAULT_CAPACITY;
            newThr = (int) (DEFAULT_LOAD_FACTOR * DEFAULT_CAPACITY);
        }
        if (newThr == 0) {
            float ft = (float) newCap * DEFAULT_LOAD_FACTOR;
            newThr = (newCap < MAXIMUM_CAPACITY && ft < (float) MAXIMUM_CAPACITY ?
                    (int) ft : Integer.MAX_VALUE);
        }
        threshold = newThr;
        Node<K, V>[] newTab = createArray(newCap);
        table = newTab;
        if (oldTab != null) {
            for (int j = 0; j < oldCap; ++j) {
                Node<K, V> e;
                if ((e = oldTab[j]) != null) {
                    oldTab[j] = null;
                    if (e.next == null)
                        newTab[e.hash & (newCap - 1)] = e;
                    else {
                        Node<K, V> loHead = null, loTail = null;
                        Node<K, V> hiHead = null, hiTail = null;
                        Node<K, V> next;
                        do {
                            next = e.next;
                            if ((e.hash & oldCap) == 0) {
                                if (loTail == null)
                                    loHead = e;
                                else
                                    loTail.next = e;
                                loTail = e;
                            } else {
                                if (hiTail == null)
                                    hiHead = e;
                                else
                                    hiTail.next = e;
                                hiTail = e;
                            }
                        } while ((e = next) != null);
                        if (loTail != null) {
                            loTail.next = null;
                            newTab[j] = loHead;
                        }
                        if (hiTail != null) {
                            hiTail.next = null;
                            newTab[j + oldCap] = hiHead;
                        }
                    }
                }
            }
        }
        return newTab;
    }

    @SuppressWarnings("unchecked")
    private Node<K, V>[] createArray(int capacity) {
        return (Node<K, V>[]) new Node[capacity];
    }

    @Data
    @AllArgsConstructor
    private static class Node<K, V> implements Entry<K, V> {

        private final int hash;
        private final K key;
        private V value;
        private Node<K, V> next;

        @Override
        public String toString() {
            return key + "=" + value;
        }
    }
}
