package org.map;

import java.util.HashMap;
import java.util.Map;

/**
 * Not final result.
 * In some places collections do not work correctly.
 */
public class SpeedTest {

    private static final int N = 10_000;

    public static void main(String[] args) {
        //System.out.println("LiteHashMap: " + speedOfLiteHashMap());
        System.out.println("IterableLiteHashMap: " + speedOfIterableLiteHashMap());
        System.out.println("HashMap: " + speedOfHashMap());
    }

    /*
    The average:
    LiteHashMap: 2.265500 ms,
    IterableLiteHashMap: 80.936680 ms,
    HashMap: 2.415030 ms.

    After update 1:
    LiteHashMap: does not work,
    IterableLiteHashMap: 30.378887 ms,
    HashMap: 2.811580 ms.
     */

    @Deprecated
    private static long speedOfLiteHashMap() {
        long before = System.nanoTime();
        LiteMap<String, Integer> map = new LiteHashMap<>();
        for (int i = 0; i < N; i++) {
            map.put((String.valueOf(i)), i);
        }
        long after = System.nanoTime();
        System.out.println(map);
        return after - before;
    }

    private static long speedOfIterableLiteHashMap() {
        long before = System.nanoTime();
        IterableMap<String, Integer> map = new IterableLiteHashMap<>();
        for (int i = 0; i < N; i++) {
            map.put((String.valueOf(i)), i);
        }
        long after = System.nanoTime();
        System.out.println(map.size());
        return after - before;
    }

    private static long speedOfHashMap() {
        long before = System.nanoTime();
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < N; i++) {
            map.put((String.valueOf(i)), i);
        }
        long after = System.nanoTime();
        System.out.println(map.size());
        return after - before;
    }
}
