package org.map;

import org.junit.Assert;
import org.junit.Test;
import org.map.LiteHashMap;
import org.map.LiteMap;

import java.util.HashMap;
import java.util.Map;

public class LiteMapTest {

    @Test
    public void testSize() {
        LiteMap<String, Integer> map = new LiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertEquals(3, map.size());
    }

    @Test
    public void testIsEmpty() {
        LiteMap<String, Integer> map = new LiteHashMap<>();
        Assert.assertTrue(map.isEmpty());
    }

    @Test
    public void testContainsKey() {
        LiteMap<String, Integer> map = new LiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertTrue(map.containsKey("One"));
        Assert.assertFalse(map.containsKey("Four"));
    }

    @Test
    public void testContainsValue() {
        LiteMap<String, Integer> map = new LiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertTrue(map.containsValue(3));
        Assert.assertFalse(map.containsValue(4));
    }

    @Test
    public void testGet() {
        LiteMap<String, Integer> map = new LiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertEquals(new Integer(3), map.get("Three"));
    }

    @Test
    public void testPut() {
        LiteMap<String, Integer> map = new LiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        map.put("Three", 33);
        Assert.assertEquals(3, map.size());
        Assert.assertNull(map.put("Four", 4));
        Assert.assertNotNull(String.valueOf(1), map.put("One", 11));
        Assert.assertEquals(4, map.size());
    }

    @Test
    public void testRemove() {
        LiteMap<String, Integer> map = new LiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertEquals(3, map.size());
        Assert.assertEquals(new Integer(2), map.remove("Two"));
        Assert.assertNull(map.remove("Four"));
        Assert.assertEquals(2, map.size());
    }

    @Test
    public void testPutAll() {
        Map<String, Integer> map = new HashMap<>();
        map.put("Zero", 0);
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        LiteMap<String, Integer> newMap = new LiteHashMap<>();
        Assert.assertTrue(newMap.isEmpty());
        newMap.putAll(map);
        Assert.assertEquals(map.size(), newMap.size());
    }

    @Test
    public void testClear() {
        LiteMap<String, Integer> map = new LiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertFalse(map.isEmpty());
        map.clear();
        Assert.assertTrue(map.isEmpty());
    }
}