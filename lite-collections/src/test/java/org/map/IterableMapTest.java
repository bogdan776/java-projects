package org.map;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class IterableMapTest {

    @Test
    public void testIterator() {
        IterableMap<String, Integer> map = new IterableLiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        map.put("Four", 4);
        for (LiteMap.Entry<String, Integer> entry : map) {
            Assert.assertNotNull(entry);
        }
    }

    @Test
    public void testSize() {
        LiteMap<String, Integer> map = new IterableLiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertEquals(3, map.size());
    }

    @Test
    public void testIsEmpty() {
        LiteMap<String, Integer> map = new IterableLiteHashMap<>();
        Assert.assertTrue(map.isEmpty());
    }

    @Test
    public void testContainsKey() {
        LiteMap<String, Integer> map = new IterableLiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertTrue(map.containsKey("One"));
        Assert.assertFalse(map.containsKey("Four"));
    }

    @Test
    public void testContainsValue() {
        LiteMap<String, Integer> map = new IterableLiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertTrue(map.containsValue(3));
        Assert.assertFalse(map.containsValue(4));
    }

    @Test
    public void testGet() {
        LiteMap<String, Integer> map = new IterableLiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertEquals(new Integer(3), map.get("Three"));
    }

    @Test
    public void testPut() {
        LiteMap<String, Integer> map = new IterableLiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertEquals(3, map.size());
        Assert.assertNull(map.put("Four", 4));
        Assert.assertEquals(new Integer(2), map.put("Two", 22));
        Assert.assertEquals(new Integer(22), map.get("Two"));
        Assert.assertEquals(4, map.size());
    }

    @Test
    public void testRemove() {
        LiteMap<String, Integer> map = new IterableLiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertEquals(3, map.size());
        Assert.assertEquals(new Integer(2), map.remove("Two"));
        Assert.assertEquals(2, map.size());
    }

    @Test
    public void testPutAll() {
        Map<String, Integer> map = new HashMap<>();
        map.put("Zero", 0);
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        LiteMap<String, Integer> newMap = new IterableLiteHashMap<>();
        Assert.assertTrue(newMap.isEmpty());
        newMap.putAll(map);
        Assert.assertEquals(map.size(), newMap.size());
    }

    @Test
    public void testClear() {
        LiteMap<String, Integer> map = new IterableLiteHashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);
        Assert.assertFalse(map.isEmpty());
        map.clear();
        Assert.assertTrue(map.isEmpty());
    }
}
