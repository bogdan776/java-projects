package com.example;

import com.example.entity.Customer;
import com.example.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Slf4j
public class Runner {

    public static void main(String[] args) {
        SpringApplication.run(Runner.class);
    }

    @Bean
    public CommandLineRunner commandLineRunner(CustomerRepository repository) {
        return (args -> {
            repository.save(new Customer("One", "First", "active"));
            repository.save(new Customer("Two", "Second", "active"));
            repository.save(new Customer("Three", "Third", "active"));
            repository.save(new Customer("Four", "Fourth", "blocked"));
            repository.save(new Customer("One`s brother", "First", "blocked"));

            log.info("All users:");
            for (Customer customer : repository.findAll()) {
                log.info(customer.toString());
            }

            log.info("Find by id");
            Customer customerById = repository.findById(1L).get();
            log.info(customerById.toString());

            log.info("Find by first name");
            repository.findByFirstName("One").forEach(c -> log.info(c.toString()));

            log.info("Find by last name");
            repository.findByLastName("First").forEach(c -> log.info(c.toString()));

            log.info("Find blocked users");
            repository.findByParam("blocked").forEach(c -> log.info(c.toString()));
        });
    }
}
