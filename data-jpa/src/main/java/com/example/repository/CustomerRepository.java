package com.example.repository;

import com.example.entity.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByFirstName(String firstName);

    List<Customer> findByLastName(String lastName);

    Optional<Customer> findById(Long id);

    @Query("select c from Customer c where c.status = :status")
    List<Customer> findByParam(@Param("status") String status);
}
