package org.example.itextpdf;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.SneakyThrows;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ITextPdfWriter {

    public static void write(String fileName, String content) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(fileName));
        document.open();
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.BLACK);
        Chunk chunk = new Chunk(content, font);
        chunk.setLineHeight(20);
        Paragraph paragraph = new Paragraph(10, chunk);
        document.add(paragraph);
        document.close();
    }

    public static void addImage(String imagePath, String pdfPath, float scalePercent)
            throws IOException, DocumentException {
        Path path = Paths.get(imagePath);
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(pdfPath));
        Image image = Image.getInstance(path.toAbsolutePath().toString());
        image.scalePercent(scalePercent);
        document.open();
        document.add(image);
        document.close();
    }

    public static void addTable(String outFile) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(outFile));
        document.open();
        PdfPTable table = new PdfPTable(3);
        addTableHeader(table, "Column One", "Column Two", "Column Three");
        addRows(table, "row 1, col 1", "row 1, col 2", "row 1, col 3");
        addCustomRows(table);
        document.add(table);
        document.close();
    }

    public static void security(String input, String output,
                                String userPassword, String ownerPassword, int permissions, int encryptionType)
            throws DocumentException, IOException {
        PdfReader pdfReader = new PdfReader(input);
        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileOutputStream(output));
        pdfStamper.setEncryption(userPassword.getBytes(), ownerPassword.getBytes(), permissions, encryptionType);
        pdfStamper.close();
    }

    private static void addTableHeader(PdfPTable table, String... context) {
        Stream.of(context)
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.ORANGE);
                    header.setBorderWidth(1);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private static void addRows(PdfPTable table, String... context) {
        for (String str : context) {
            table.addCell(str);
        }
    }

    private static void addCustomRows(PdfPTable table) throws BadElementException, IOException {
        Path path = Paths.get("input-data/java-logo.png");
        Image img = Image.getInstance(path.toAbsolutePath().toString());
        img.scalePercent(15);
        PdfPCell imageCell = new PdfPCell(img);
        table.addCell(imageCell);

        PdfPCell horizontalAlignCell = new PdfPCell(new Phrase("row 2, col 2"));
        horizontalAlignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(horizontalAlignCell);

        PdfPCell verticalAlignCell = new PdfPCell(new Phrase("row 2, col 3"));
        verticalAlignCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
        table.addCell(verticalAlignCell);
    }

    @SneakyThrows
    private static void setPassword() {
        security("output-data/iText.pdf", "output-data/security.pdf",
                "user", "admin", PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_256);
    }

    @SneakyThrows
    private static void table() {
        addTable("output-data/security.pdf");
    }
}
