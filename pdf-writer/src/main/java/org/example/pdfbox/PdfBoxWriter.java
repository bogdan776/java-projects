package org.example.pdfbox;

import lombok.SneakyThrows;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.util.Reader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PdfBoxWriter {

    public static void write(String filePath, String context) throws IOException {
        PDDocument document = new PDDocument();
        PDPage page = new PDPage();
        document.addPage(page);
        PDPageContentStream contentStream = new PDPageContentStream(document, page);

        contentStream.beginText();
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
        contentStream.setLeading(20);
        contentStream.newLineAtOffset(25, 750);

        String[] lines = context.split(System.lineSeparator());

        for (String line : lines) {
            contentStream.showText(line);
            contentStream.newLine();
        }
        contentStream.endText();

        contentStream.close();
        document.save(filePath);
        document.close();
    }

    public static void drawImage(String imgPath, String outFilePath) throws IOException {
        PDDocument document = new PDDocument();
        PDPage page = new PDPage();
        document.addPage(page);
        Path path = Paths.get(imgPath);
        PDPageContentStream contentStream = new PDPageContentStream(document, page);
        PDImageXObject image = PDImageXObject.createFromFile(path.toAbsolutePath().toString(), document);

        int width = image.getWidth() / 2;
        int height = image.getHeight() / 2;

        contentStream.drawImage(image, 50f, 500f, width, height);
        contentStream.close();
        document.save(outFilePath);
        document.close();
    }

    public static void setPassword(String filePath, String ownerPassword, String userPassword, boolean print)
            throws IOException {
        PDDocument document = new PDDocument();
        PDPage page = new PDPage();
        document.addPage(page);

        AccessPermission accessPermission = new AccessPermission();
        accessPermission.setCanPrint(print);
        accessPermission.setCanModify(false);

        StandardProtectionPolicy standardProtectionPolicy
                = new StandardProtectionPolicy(ownerPassword, userPassword, accessPermission);
        document.protect(standardProtectionPolicy);
        document.save(filePath);
        document.close();
    }

    @SneakyThrows
    private static String readTextFromFile(String fileName) {
        return Reader.read(fileName);
    }

    @SneakyThrows
    private static void write() {
        String context = readTextFromFile("input-data" + File.separator + "text2.txt");
        write("output-data/pdfBoxText.pdf", context);
    }

    @SneakyThrows
    private static void drawImage() {
        drawImage("input-data" + File.separator + "java-logo.png", "output-data/pdfBoxImage.pdf");
    }

    @SneakyThrows
    private static void setPassword() {
        setPassword("output-data" + File.separator + "pdfBoxEncryption.pdf",
                "admin", "user", false);
    }
}
