package org.util;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;

public class ReaderTest {

    @SneakyThrows
    @Test
    public void testRead() {
        String textFromFile = Reader.read("../input-data/test.txt");
        Assert.assertEquals("Hello Reader!", textFromFile);
    }
}